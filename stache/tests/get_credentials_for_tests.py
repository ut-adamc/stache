from __future__ import print_function

import functools
import os
import sys

def dict_as_object(adict):
    class DictProxy:
        pass

    proxy = DictProxy()
    for k,v in adict.items():
        setattr(proxy, k, v)
    return proxy

def find_config():
    first_print = functools.partial(print, '\n\n')

    def nop(**kwargs): #pylint: disable=unused-argument
        pass

    def separate_on_console(**kwargs):
        nonlocal first_print
        first_print(**kwargs)
        first_print = nop

    def message(msg, **kwargs):
        separate_on_console(**kwargs)
        print(msg, **kwargs)

    try:
        import yaml
    except ImportError:
        message('PyYAML must be installed to run tests.',
                file=sys.stderr
                )
        return None

    config_path = os.environ.get('STACHE_TEST_CONFIG')
    message('>> STACHE_TEST_CONFIG: ' + str(config_path or ''))
    if config_path and not os.path.exists(config_path):
        message('Unable to use the configuration path '
                'specified in the environment variable '
                'STACHE_TEST_CONFIG'
                )
        return None  # we bail if you specified a path that doesn't exist

    if config_path is None:
        config_path = os.path.join(os.getcwd(), 'test_config.yaml')
        if not os.path.exists(config_path):
            config_path = ''
            while not os.path.exists(config_path):
                if config_path != '':
                    print('> "{0}" is not a valid path.'.format(config_path),
                          file=sys.stderr)

                separate_on_console()
                config_path = input('Enter a test config path or enter for no ')
                if config_path == '':
                    break

    if config_path == '':
        return None
    else:
        with open(config_path, 'r') as f:
            config = yaml.safe_load(f)
            separate_on_console()
            print('> Using Configuration file at "{0}"\n'.format(config_path))
            return dict_as_object(config)


def get_config():
    config = find_config()
    if config:
        return config
    else:
        return None
