from stache import api_request

from stache.errors import StacheValueError


def parse_secret_as_dict(secret, key):
    '''
    Parse out secret to only return requested environment.
    Environment is just the key of the dictionary in stache.
    Expected Format in Secret:

    env: password123
    env: passwordABC

    '''
    if key is None:
        raise StacheValueError('Cannot parse dict with a key of None.')

    list_envs = [x.strip() for x in secret.split('\n')]

    secret_dict = {}
    for y in list_envs:
        piece = [x.strip() for x in y.split(':', 1)]
        secret_dict[piece[0]] = piece[1]

    password = secret_dict[key]
    return password

def simple_secret(secret, key): #pylint: disable=unused-argument
    return secret


class GetCredential(object):
    def __init__(self, api_key, item_id, env=None, *, endpoint_url=None):

        if item_id is None and endpoint_url is None:
            raise ValueError('You must supply item_id (api v1) or '
                             'endpoint_url.')

        self.api_key = api_key

        if endpoint_url is None:
            self.endpoint_url = api_request.create_v1_url(item_id)
        else:
            self.endpoint_url = endpoint_url

        self.env = env

    @classmethod
    def from_endpoint(cls, *, endpoint_url, x_stache_key, env=None):
        """Virtual constructor to make endpoints more convenient."""
        cred = GetCredential(x_stache_key, None, env, endpoint_url=endpoint_url)
        return cred

    def ret_creds(self, **kwargs):
        """
        parameters:
        secret_parser defaults to parse_secret_as_dict (historical)
        kwargs: Named Parameters to feed to parse_secret (secret_parser and/or
            secret_key).
        """
        # leave variables labeled as documeentation.
        #pylint: disable=unused-variable
        (purpose,
         secret,
         nickname,
         memo) = api_request.retrieve_from_endpoint(
             endpoint_url=self.endpoint_url,
             x_stache_key=self.api_key
        )

        secret_val = self.parse_secret(secret, **kwargs)
        return nickname, secret_val

    def parse_secret(self, secret, *, secret_parser=None, secret_key=None):
        """
        parameters:
        secret: the secret value from stache.
        secret_parser: the function that parses the secret. If None,
            parse_secret_as_dict is used.
        secret_key: the key to use to parse the secret -- if None,
            self.env will be used.
        """
        if secret_parser is None:
            secret_parser = parse_secret_as_dict

        if secret_key is None:
            secret_key = self.env

        return secret_parser(secret, secret_key)
