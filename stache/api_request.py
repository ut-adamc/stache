from collections import OrderedDict
import json
import logging
import warnings
import re
import time

import requests

from stache.errors import StacheFormatError, TryLimitError

comment_template = '^([^{comment_char}]*){comment_char}?.*$'


DEFAULT_TRY_LIMIT = 3
TRY_MAX = 20 # sanity check

DEFAULT_TRY_SLEEP = 1.0 # in seconds
TRY_SLEEP_MAX = 60.0    # in seconds; sanity check

STACHE_PROTOCOL_HOST = "https://stache.utexas.edu"
V1_URL = f"{STACHE_PROTOCOL_HOST}/api/v1/item/read/"

LOGGER = logging.getLogger(__name__)
LOGGER.addHandler(logging.NullHandler())


def create_v1_url(item_id):
    # essentially, the via formula
    url = V1_URL + item_id
    return url


def retrieve_creds(api_key, item_id, *,
                   try_limit=DEFAULT_TRY_LIMIT,
                   try_sleep=DEFAULT_TRY_SLEEP):
    """Note that this function only works with v1 urls, and is therefore
       deprecated."""

    warnings.warn('This method is deprecated, as it only works for version '
                  '"v1" of the API. Consider retrieve_from_endpoint.'
    )

    return retrieve_from_endpoint(endpoint_url=create_v1_url(item_id),
                                  x_stache_key=api_key,
                                  try_limit=try_limit,
                                  try_sleep=try_sleep
    )


def retrieve_from_endpoint(*, endpoint_url, x_stache_key,
                           try_limit=DEFAULT_TRY_LIMIT,
                           try_sleep=DEFAULT_TRY_SLEEP):
    """Retrieve from stache using the API endpoint and the x_stache_key.
       (The x_stache_key is equivalent to what used to be called the
       api_key in stache.)"""

    if try_limit < 1:
        raise ValueError("try_limit must be at least 1.")

    if try_limit > TRY_MAX:
        raise ValueError("try_limit is too high; {0} is the max allowed."
                         .format(TRY_MAX)
        )
    if try_sleep > TRY_SLEEP_MAX:
        raise ValueError("try_sleep is too high; {0} is the max allowed."
                         .format(TRY_SLEEP_MAX)
        )

    if endpoint_url.startswith('/'): # path -- what stache app shows currently
        endpoint_url = STACHE_PROTOCOL_HOST + endpoint_url

    headers = {'X-STACHE-READ-KEY': x_stache_key}

    tries = 0
    while tries < try_limit:
        tries += 1
        LOGGER.info('attempting to call stache API; tries=%s; try_limit=%s',
                    tries,
                    try_limit
        )
        try:
            response = requests.get(endpoint_url, headers=headers)
        except Exception:
            LOGGER.error('stache API call failed with exception',
                         exc_info=True
            )
            raise

        if response.ok:
            return parse_response(response.content)
        else:
            LOGGER.warning('stache call failed with status code %s',
                           response.status_code
            )
            if response.status_code == 429:
                # request limited to two per second, so sleep a second
                LOGGER.info('sleeping for %s seconds before retry.',
                            try_sleep
                )
                time.sleep(try_sleep)
                continue
            else:
                response.raise_for_status()

    # if we got here, exceeded the retry_limit
    LOGGER.info('try_limit of %s exceeded; raising exception')
    raise TryLimitError('try limit of {0} exceeded.'
                        .format(try_limit)
    )


def parse_response(response):
    try:
        jData = json.loads(response.decode('utf-8'))
        results= (jData['purpose'],
                  jData['secret'],
                  jData['nickname'],
                  jData['memo'],
        )
        return results
    except Exception as ex:
        LOGGER.error('Unable to parse_response', exc_info=True)
        raise StacheFormatError('Unable to decode stache entry') from ex

def parse_field_as_dict(field, *,
                        key_delimiter=':',
                        key_repeats=True,
                        value_delimiter=None,
                        default_value=None,
                        comment_char='#'):
    try:
        results = _parse_field_as_dict(field,
                                       key_delimiter=key_delimiter,
                                       key_repeats=key_repeats,
                                       value_delimiter=value_delimiter,
                                       default_value=default_value,
                                       comment_char=comment_char)
        return results
    except ValueError:
        LOGGER.error('Unable to parse_field_as_dict', exc_info=True)
        raise # preserve current ValueErrors

    except Exception as ex:
        LOGGER.error('Unable to parse_field_as_dict', exc_info=True)
        raise StacheFormatError('Unable to parse stache entry') from ex



def _parse_field_as_dict(field, *,
                        key_delimiter=':',
                        key_repeats=True,
                        value_delimiter=None,
                        default_value=None,
                        comment_char='#'):
    '''
    Parse field as an OrderedDict, where each line is a key: value pair.
    Params:
      field: A string to be parsed.
      key_delimiter: The character separating the key from the value. CANNOT
        BE NONE! Defaults to ':'.
      key_repeats: If true, it is assumed that repeated key:value pairs
        should be treated as multiple values for the key, to be returned
        in the order listed. If false-y, a repeated key will replace the
        prior value. Defaults to True.
      value_delimiter: If None, a single value is assumed. If set, the value
        WILL ALWAYS be a tuple, regardless of how many values were found.
        Defaults to None.
      default_value: The value to assume if we only find a key without a value.
        Defaults to None.
      comment_char: The character that starts line-comments (which are NOT
        parsed). Defaults to '#'.

    Leading and trailing whitespace in keys and values will be ignored.
    '''
    if key_delimiter is None:
        raise ValueError('key_delimiter may not be None.')

    lines = [line.strip() for line in field.split('\n')]

    if comment_char is not None:
        comment_pattern = (comment_template
                           .format(comment_char=re.escape(comment_char))
        )
        comment_re = re.compile(comment_pattern)

    fd = OrderedDict()
    for line in lines:
        if comment_char is not None and comment_char in line:
            m = comment_re.match(line)
            if m:
                line = m.groups(1)[0]
            else:
                raise ValueError('comment_char in line but re does not match.')

        if key_delimiter in line:
            key, rest = [v.strip() for v in line.split(key_delimiter, 1)]
            if value_delimiter is not None:
                # if value_delimiter was set, the value will always be a tuple,
                # regardless of how many values there are
                value = list((v.strip() for v in rest.split(value_delimiter)))
            else:
                value = rest
        else:
            key = line.strip()
            value = default_value

        if key == '': # blank keys (empty lines, comment lines, etc.) ignored
            continue
        else:
            if key in fd and key_repeats:
                values = fd[key]
                if not isinstance(values, list):
                    values = [values]
                    fd[key] = values
                values.append(value)
            else:
                fd[key] = value

    return fd
